#!/usr/bin/env node

require('dotenv').config();

const yargs = require('yargs')
.check(async argv => {
    const package = require(process.cwd() +'/package.json');
    if (!package.hasOwnProperty('version')) {
      throw new Error('Cannot find the package.version');
    }
}, true)
.usage('Usage: gcp-kubernetes <command> [options]')
  .env('GCLOUD')
  .demandCommand(1, 'You need at least one command')
  .help('h').alias('h', 'help')

  .command('image-build', 'Build Image', yargs => {
    yargs
      .option('i', {nargs: 1, alias: 'image-name', desc: 'The name of the image to build, without registry, project id or tag'})
      .demandOption(['i'])
    ;
  }, require('./image-build'))

  .command('image-push', 'Push Image to GCR', yargs => {
    yargs
      .option('i', {nargs: 1, alias: 'image-name', desc: 'The name of the image to push, without registry, project id or tag'})
      .option('r', {nargs: 1, alias: 'registry-region', desc: 'The region where the image registry reside'})
      .option('c', {nargs: 1, alias: 'cluster', desc: 'The cluster name will be used as a folder in the image registry'})
      .option('f', {nargs: 1, alias: 'api-keyfile', desc: 'The GCLOUD API JSON key file path'})
      .option('p', {nargs: 1, alias: 'project-id', desc: 'The GCLOUD project id'})
      .demandOption(['i', 'r', 'c', 'f', 'p'])
    ;
  }, require('./image-push'))

  .command('image-deploy', 'Deploy Image to GKE', yargs => {
    yargs
      .option('i', {nargs: 1, alias: 'image-name', desc: 'The name of the image to build'})
      .option('r', {nargs: 1, alias: 'registry-region', desc: 'The region where the image registry reside'})
      .option('c', {nargs: 1, alias: 'cluster', desc: 'The cluster name'})
      .option('g', {nargs: 1, alias: 'cluster-region', desc: 'The cluster region'})
      .option('f', {nargs: 1, alias: 'api-keyfile', desc: 'The GCLOUD API JSON key file path'})
      .option('p', {nargs: 1, alias: 'project-id', desc: 'The GCLOUD project id'})
      .demandOption(['i', 'r', 'c', 'g', 'f', 'p'])
    ;
  }, require('./image-deploy'))

;

// This is required to process the command...
const argv = yargs.argv; // eslint-disable-line no-unused-vars
