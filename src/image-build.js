
const shell = require('child_process').execSync;
const logger = require('./logger');
const helper = require('./helper');

module.exports = async argv => {
  try {
    await helper.auth(argv.projectId, argv.apiKeyfile);

    const imageRegistry = helper.getImageRegistry(argv);
    const imageExists = await helper.registryImageAlreadyExists(imageRegistry);
    if (imageExists) {
      logger.error(`Docker Image ${imageRegistry} already exists, please increment the version in your package.json`);
      process.exit(1);
      return;
    }

    shell(`docker build -t ${imageRegistry} .`, {stdio: 'inherit'});

  } catch (err) {
    logger.error(err.message);
    process.exit(1);
  }
};
