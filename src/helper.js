
const fs = require('fs');
const util = require('util');
const shell = util.promisify(require('child_process').exec);
const readFile = util.promisify(fs.readFile);
const writeFile = util.promisify(fs.writeFile);
const yaml = require('js-yaml');
const semver = require('semver');

const logger = require('./logger');
const packageJson = require(process.cwd() +'/package.json');

module.exports = {
  getDockerVersion: async _ => {
    let { stdout } = await shell("docker version -f '{{.Client.Version}}'");

    logger.info(`Docker raw version: ${stdout}`);

    // Bug in Semver with leading zeros
    stdout = stdout.split('.').map(num => {
      return num.replace(/^0+(\d+$)/, '$1');
    }).join('.');

    return semver.coerce(stdout).raw;
  },
  auth: async (projectId, apiKeyfile) => {
    await shell(`gcloud auth activate-service-account --key-file ${apiKeyfile}`);
    await shell(`gcloud config set project ${projectId}`);
  },
  getImageSource: argv => {
    return `${argv.imageName}:${packageJson.version}`;
  },
  getImageRegistry: argv => {
    return `${argv.registryRegion}/${argv.projectId}/${argv.imageName}:${packageJson.version}`;
  },
  registryImageAlreadyExists: async (image) => {
    try {
      logger.info(`gcloud container images describe ${image}`);
      const { stdout } = await shell(`gcloud container images describe ${image} --format json`);
      logger.info(`registryImageAlreadyExists output: ${stdout}`);
    } catch (err) {
      logger.info(`registryImageAlreadyExists failed: ${JSON.stringify(err)}`);
      return false;
    }

    return true;
  },
  findClusterInRegions: async (cluster, region) => {
    const { stdout } = await shell(`gcloud compute regions describe ${region}`);

    let zones = [];
    yaml.safeLoad(stdout).zones.forEach(item => {
      zones.push(item.match(/[a-z0-9-]+$/)[0]);
    });

    const zonesFound = [];
    for (let i in zones) {
      const zone = zones[i];
      try {
        const { stdout } = await shell(`gcloud container clusters describe ${cluster} --zone ${zone}`);
        zonesFound.push(yaml.safeLoad(stdout));
      } catch (err) {}
    }

    return zonesFound;
  },
  createK8sCluster: async (region, name) => {
    throw new Error('Not Implemented');
    // await shell(`gcloud container clusters create ${name} --zone `);
  },
  updateDeploymentImage: async (newImage, imageName) => {
    const cwd = process.cwd();
    const k8sDir = `${cwd}/k8s/master`;
    const file = `${k8sDir}/app.yml`;
    const fileBuffer = await readFile(file);
    let documents = yaml.safeLoadAll(fileBuffer.toString());

    // Find the image of the project
    const deployment = documents.find(doc => doc.kind === 'Deployment');
    let appContainer = deployment.spec.template.spec.containers.find(container => container.name === imageName);
    if (!appContainer) {
      throw new Error(`Cannot find container image with name "${imageName}" from ${file}`);
    }

    appContainer.image = newImage;
    logger.info(`Updated old deployment image version to: ${newImage}`);
    await writeFile(file, documents.map(doc => yaml.dump(doc)).join("\n---\n"));
  },
};
