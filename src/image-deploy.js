
const fs = require('fs');
const shell = require('child_process').execSync;
const logger = require('./logger');
const helper = require('./helper');

module.exports = async argv => {

  try {
    const imageRegistry = helper.getImageRegistry(argv);

    await helper.auth(argv.projectId, argv.apiKeyfile);

    const targetedRegions = argv.clusterRegion.split(',');
    const clusters = await helper.findClusterInRegions(argv.cluster, targetedRegions);
    if (clusters.length !== targetedRegions.length) {
      logger.error(`Cluster ${argv.cluster} was not found in region(s) ${argv.clusterRegion}`);
      return false;
    }

    // We update the deployment file with the image we want to deploy
    await helper.updateDeploymentImage(imageRegistry, argv.imageName);

    const cwd = process.cwd();
    for (let i in clusters) {
      const cluster = clusters[i];

      const k8sAppDir = `${cwd}/k8s/${cluster.name}`;
      if (!fs.existsSync(k8sAppDir)) {
          logger.error(`Could not find kubernetes config files in '${k8sAppDir}' for cluster ${cluster.name}`);
          return false;
      }

      logger.info(`gcloud container clusters get-credentials ${cluster.name} -z ${cluster.zone}`);
      shell(`gcloud container clusters get-credentials ${cluster.name} -z ${cluster.zone}`, {stdio: 'inherit'});

      // If we have a secrets repository we clone it
      if (argv.hasOwnProperty('secretsRepo') && argv.secretsRepo) {
        logger.info(`git clone ${argv.secretsRepo} k8s/secrets`);
        shell(`git clone ${argv.secretsRepo} k8s/secrets`, {stdio: 'inherit'});

        // If there are secrets for this cluster we create/update them
        let secretDir = `${cwd}/k8s/secrets/${cluster.name}`;
        if (fs.existsSync(secretDir)) {
          logger.info(`kubectl apply -f ${secretDir}`);
          shell(`kubectl apply -f ${secretDir}`, {stdio: 'inherit'});
        }
      }

      logger.info(`kubectl apply -f ${k8sAppDir}`);
      shell(`kubectl apply -f ${k8sAppDir}`, {stdio: 'inherit'});
    }

  } catch (err) {
    logger.error(err.message);
    process.exit(1);
  }
};
