
const shell = require('child_process').execSync;
const semver = require('semver');
const logger = require('./logger');
const helper = require('./helper');

module.exports = async argv => {
  try {
    const dockerVersion = await helper.getDockerVersion();
    if (!semver.valid(dockerVersion)) {
      logger.error(`Could not parse docker version`);
      process.exit(1);
      return;
    }

    const imageRegistry = helper.getImageRegistry(argv);

    await helper.auth(argv.projectId, argv.apiKeyfile);

    // For docker versions less than 18 we need a different authentication mechanism
    if (semver.lt(dockerVersion, '18.0.0')) {
      shell(`gcloud docker --verbosity=error -- push ${imageRegistry}`, {stdio: 'inherit'});
    } else {
      shell('echo Y | gcloud auth configure-docker --verbosity=error', {stdio: 'inherit'});
      shell(`docker push ${imageRegistry}`, {stdio: 'inherit'});
    }

  } catch (err) {
    logger.error(err.message);
    process.exit(1);
  }
};
