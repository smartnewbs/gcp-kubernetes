# Google Cloud Platform & Kubernetes cluster
The library is meant to be used in concert with [Atlassian Bitbucket piplines](https://bitbucket.org/product/features/pipelines).

Although it is meant to only deploy the "app" and not its database, it can be refined to any infrastructure need.

It expect certain environment variables to be set
```bash
GCLOUD_API_KEYFILE=./gcloud-api-key.json
GCLOUD_PROJECT_ID=boilerplates-190517

GCLOUD_CLUSTER=master
GCLOUD_CLUSTER_REGION=northamerica-northeast1

BITBUCKET_COMMIT=sfn24d9823i953id98
BITBUCKET_BRANCH=BOIL-432

GCLOUD_REGISTRY_REGION=us.gcr.io
GCLOUD_IMAGE_NAME=feathersjs
```

It also expect that the *IAM & Admin* to have the following roles:
```
Compute Engine -> Compute Viewer
Kubenetes Engine -> Kubernetes Engine Admin
Service Accounts -> Service Account User
Storage -> Storage Admin
```
